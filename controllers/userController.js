const bcrypt = require('bcrypt');
const User = require('./../models/User');

// @path    /users/register
// @method  POST
// @desc    Create User
// @privacy public
// @body
    // email    password
// @return  Boolean
module.exports.register = (req, res) => {

    // Validate password - min 8 charachters
    if (req.body.password.length < 8) return res.send(false);

    // Confirm password
    if (req.body.password !== req.body.confirmPassword) return res.send(false);

    // hash password from request body
    const hash = bcrypt.hashSync(req.body.password, 9);
    // reassign valut of req.body.password to the created hashed password
    req.body.password = hash;
    
    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password,
        mobileNo: req.body.mobileNo
    });

    newUser.save()
    .then(() => res.send(true))
    .catch(() => res.send(false));

}

// @path    /users/login
// @method  POST
// @desc    Authenticate User
// @privacy public
// @body    email password
// return   token or false
module.exports.login = (req, res) => {
    res.send('This is login route');
};

// @path    /users/details
// @method  GET
// @desc    get authenticated user's details
// @privacy private
// @return  user details or false(401)
module.exports.getDetails = (req, res) => {
    res.send('Logged User Details');
};