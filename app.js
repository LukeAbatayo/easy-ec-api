const express = require('express');
const mongoose = require('mongoose');

// Initialize Express
const app = express();

// Set Port
const port = process.env.POST || 4000;

// Database Connection
mongoose.connect('mongodb+srv://lukeabatayo:010499Jonna@cluster0.cmkqe.mongodb.net/easy-ec-api?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})
.then(() => {
    console.log("Connected to database");
})
.catch(() => {
    console.log(err.message);
});

// Request Body Parser
app.use(express.json());

// Define Routes
// Users Routes
const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);

// Port Assignment
app.listen(port, () => {
    console.log(`Listening to port ${port}`);
});