const router = require('express').Router();
const {register, login, getDetails} = require('./../controllers/userController');

router.post('/register', register);
router.post('/login', login);
router.post('/details', getDetails);

module.exports = router;